//
//  GameScene.swift
//  ZombieConga
//
//  Created by Parrot on 2019-01-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
   
    // MARK: Sprites
    // -------------------------
    var zombie:SKSpriteNode!
    var grandma:SKSpriteNode!
    var livesLabel:SKLabelNode!
    
    var lives = 100
    
    override func didMove(to view: SKView) {
        // Set the background color of the app
        self.backgroundColor = SKColor.white;
        
        // Add your zombie
        self.zombie = SKSpriteNode(imageNamed: "zombie1")
        self.zombie.position = CGPoint(x: 400, y: 400)
        addChild(zombie)
        
        // Add you gramma
        self.grandma = SKSpriteNode(imageNamed: "enemy")
        self.grandma.position = CGPoint(x:size.width - 100, y:size.height / 2)
        addChild(self.grandma)
        
        // Add life label
        self.livesLabel = SKLabelNode(text: "Lives Remaining: \(lives)")
        self.livesLabel.position = CGPoint(x:400, y:800)
        self.livesLabel.fontColor = UIColor.yellow
        self.livesLabel.fontSize = 65
        self.livesLabel.fontName = "Avenir"
        addChild(self.livesLabel)
        
        
        // Make gramma move using automatic movement
        let move1 = SKAction.move(to: CGPoint(x: size.width/2 , y: 400),
                                  duration: 2)
        let move2 = SKAction.move(to:CGPoint(x:100, y:size.height/2), duration:2)
        let move3 = SKAction.move(to:CGPoint(x:size.width/2, y:400), duration:2)
        let move4 = SKAction.move(to:CGPoint(x:size.width - 100, y:size.height / 2), duration:2)
        
        let grandmaAnimation = SKAction.sequence(
            [move1,move2, move3, move4]
        )
//        let zombieAnimation = SKAction.sequence(
//            [move3, move4]
//        )
        
        // make gramma move in this pattern forever
        let grandmaForeverAnimation = SKAction.repeatForever(grandmaAnimation)
        self.grandma.run(grandmaForeverAnimation)

        //self.zombie.run(zombieAnimation)
        
    }
    
    
    
    var mouseX:CGFloat = 0
    var mouseY:CGFloat = 0

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // 1. @TODO: write the code to get the mouse location
        
        // 2. @TODO: set the global mosueX and mouseY variables to the mouse (x,y)
        self.mouseX = _______
        self.mouseY = _______
    }
    
    func moveZombie(mouseXPosition:CGFloat, mouseYPostion:CGFloat) {
        
        // move the zombie towards the mouse
        // @TODO: Get the android code for moving bullet towards enemey
        // Implement the algorithm in Swift
        
    }
    
    // SAME AS UPDATEPOSITIONS
    override func update(_ currentTime: TimeInterval) {
        
        // move the zomibe
        self.moveZombie(mouseXPosition: self.mouseX, mouseYPostion: self.mouseY)
        
        // Detect when zombie and gramma collide
        if (self.zombie.frame.intersects(self.grandma.frame) == true) {
            print("\(currentTime): COLLISON!")
            self.lives = self.lives - 1
            
            // update the life counter
            self.livesLabel.text = "Lives Remaining: \(lives)"
            
        }
    }
    
    

    
    
    
    
    
    
}
